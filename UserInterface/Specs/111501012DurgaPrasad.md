#Design Spec :

#Objectives :

1. Login User Interface elements design

2. Implementation of login using the API provided

#Design :

A user can enter the username and get started with the software. 

There is no password authentication required as this requires keeping track of all users' username and password in a database.

The login button is at the top of the screen. The same button changes into signout button once a user is logged in. 

Once the user signs out, the button changes back into login button and he has to type in the username again and click log in.

Here is a sample of how the login screen will look.

#Login Page :

![](./111501012DurgaPrasad/BeforeLogin.png)


The below figure is the client class diagram, we will be implementing the User Interface with the help of an API. 

#Client Class Diagram :

![](./111501012DurgaPrasad/ClientClassDiagram.png)

#Design Choices :

1. We will not be using Google API because our software should be able to run on local network.

2. By using the API functions, we can create and administer the users whether they exist or not.

#Advantages :

Since we changed the spec from having different windows and having a separate pages for login etc. We will be using a single page for carrying all functionalities and the complexity has decreased. 


