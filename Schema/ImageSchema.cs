﻿//-----------------------------------------------------------------------
// <copyright file="ImageSchema.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
// </copyright>
// <author>
//      vishal kumar chaudhary
// </author>
// <review>
//      Libin N George
// </review>
//-----------------------------------------------------------------------

namespace Masti.Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class implementing the ISchema interface which is specific to image encoding and decoding
    /// </summary>
    public class ImageSchema : ISchema
    {
        /// <summary>
        /// Decoding the string data into tags in a dictionary
        /// </summary>
        /// <param name="data">encoded data in string format</param>
        /// <param name="partialDecoding">> Boolean if true just returns type of data (ImageProcessing or Messaging)</param>
        /// <returns> Dictionary containing key-value pairs of the encoded data</returns>
        public IDictionary<string, string> Decode(string data, bool partialDecoding)
        {
            Dictionary<string, string> tagDict = new Dictionary<string, string>();
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            string[] keyValues = data.Split(';');

            // for partial decoding returning the dictionary only containing type of data (ImageProcessing/Messaging)
            if (partialDecoding == true)
            {
                string[] type = keyValues[0].Split(':');

                // type is the first tag inserted into data while encoding.
                if (DecodeFrom64(type[0]) == "type")
                {
                    tagDict.Add(DecodeFrom64(type[0]), DecodeFrom64(type[1]));
                }
                else
                {
                    throw new System.ArgumentException("Data is corrupted as it does not contain the type of data", nameof(data));
                }

                return tagDict;
            }

            // fully decoding if the partialDecoding is false and adding into the dictionary
            foreach (string keyValue in keyValues)
            {
                tagDict.Add(DecodeFrom64(keyValue.Split(':')[0]), DecodeFrom64(keyValue.Split(':')[1]));
            }

            tagDict.Remove("type");
            return tagDict;
        }

        /// <summary>
        /// Encodes the data given into a dictionary containing tags into standard format(JSON)
        /// </summary>
        /// <param name="tagDict">Dictionary containing Tags to be encoded</param>
        /// <returns>Encoded data in string format</returns>
        public string Encode(Dictionary<string, string> tagDict)
        {
            if (tagDict == null)
            {
                throw new ArgumentNullException(nameof(tagDict));
            }

            Dictionary<string, string>.KeyCollection keys = tagDict.Keys;
            string result = string.Empty;

            // inserting the Type tag into encoding data
            result = EncodeTo64("type") + ':' + EncodeTo64("ImageProcessing") + ';';
            foreach (string key in keys)
            {
                result += EncodeTo64(key) + ':' + EncodeTo64(tagDict[key]) + ";";
            }

            return result.Substring(0, result.Length - 1);
        }

        /// <summary>
        /// Converting string to base64 Encoding
        /// </summary>
        /// <param name="toEncode"> Data to encode into Base64</param>
        /// <returns>Encoded Base64 string</returns>
        private static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        /// <summary>
        ///  Decode base64 Decoding string
        /// </summary>
        /// <param name="encodedData">Data to decode</param>
        /// <returns>Decoded ASCII string</returns>
        private static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }
    }
}
