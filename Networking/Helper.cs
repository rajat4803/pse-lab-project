﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal(29ayush@gmail.com)
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
// Rajat Sharma
// </reviewer>
//
// <copyright file="Helper.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
//      You are allowed to use the file and/or redistribute/modify as long as you preserve this copyright header and author tag.
// </copyright>
//
// <summary>
//      This File contains Helper Functions for Data Outgoing/Ingoing Component.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// Defines some helper functions that are needed by the communication class.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Takes a tcpClient and returns the IP associated with it as string.
        /// Used when we want to distinguish two tcpClients
        /// </summary>
        /// <param name="tcpClient"> Input tcpClient </param>
        /// <returns>IP:Port corresponding to input tcpClient as String.</returns>
        public static string GetEndPointAddress(TcpClient tcpClient)
        {
            if (tcpClient == null)
            {
                throw new InvalidProgramException();
            }

            return ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
        }

        /// <summary>
        /// Takes a Socket and returns the IP associated with it as string.
        /// Used when we want to distinguish two sockets.
        /// </summary>
        /// <param name="socket"> Input socket </param>
        /// <returns>IP:Port corresponding to input socket as String.</returns>
        public static string GetEndPointAddress(Socket socket)
        {
            if (socket == null)
            {
                throw new InvalidProgramException();
            }

            return ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
        }

        /// <summary>
        /// Takes a tcpClient and returns the IP:Port associated with it as string.
        /// Used when we want the IP corresponding to a tcpClient
        /// </summary>
        /// <param name="tcpClient"> Input tcpClient </param>
        /// <returns>IP corresponding to input tcpClient as String.</returns>
        public static string GetEndPoint(TcpClient tcpClient)
        {
            if (tcpClient == null)
            {
                throw new InvalidProgramException();
            }

            return ((IPEndPoint)tcpClient.Client.RemoteEndPoint).ToString();
        }

        /// <summary>
        /// Takes a Socket and returns the IP:Port associated with it as string.
        /// Used when we want the IP corresponding to a sockets.
        /// </summary>
        /// <param name="socket"> Input socket </param>
        /// <returns>IP corresponding to input socket as String.</returns>
        public static string GetEndPoint(Socket socket)
        {
            if (socket == null)
            {
                throw new InvalidProgramException();
            }

            return ((IPEndPoint)socket.RemoteEndPoint).ToString();
        }

        /// <summary>
        /// Combines two byte arrays. 
        /// </summary>
        /// <param name="first">First Byte Array</param>
        /// <param name="second">Second Byte Array</param>
        /// <returns>Returns a combined byte array</returns>
        public static byte[] Combine(byte[] first, byte[] second)
        {
            if (first == null || second == null)
            {
                throw new InvalidProgramException();
            }

            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        /// <summary>
        /// It enhances the logging facility provided by Diagnostics class by adding 
        /// some context to it.
        /// </summary>
        /// <param name="logMessage">Message to be logged.</param>
        /// <param name="logLevel">Level of Logging</param>
        /// <returns>Returns string by appending context to message.</returns>
        public static string ContextLogger(string logMessage, int logLevel)
        {
            return $"Level {logLevel}# Thread {Thread.CurrentThread.Name}# Message {logMessage}";
        }

        /// <summary>
        /// Return byte array corresponding to an integer in BigEndian format. 
        /// </summary>
        /// <param name="val"> Input Integer</param>
        /// <returns>byte conversion corresponding to an integer.</returns>
        public static byte[] GetBytes(int val)
        {
            byte[] intAsBytes = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(intAsBytes);
            }

            return intAsBytes;
        }

        /// <summary>
        /// Normally we log original messages in Logger but logging huge messages will unnecessary bloat the logs, hence we 
        /// log MD5Hash of messages above 100 length
        /// </summary>
        /// <param name="message">Input Log Message</param>
        /// <returns>Short Log Message</returns>
        public static string ShortLog(string message)
        {
            if (message == null) 
            {
                throw new InvalidProgramException();
            }

            if (message.Length <= 100) 
            {
                return message;
            }
            else
            {
                return MD5Hash(message);
            }
        }

        /// <summary>
        /// Returns an MD5Hash of a string.
        /// A message's acknowledgement is sent by sending an MD5hash of that message
        /// </summary>
        /// <param name="input">String For which MD5 hash has to be generated.</param>
        /// <returns>string containing MD5hash of input string</returns>
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            byte[] bytes;
            using (MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider())
            {
                bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));
                md5provider.Clear();
            }
            
            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }

            return hash.ToString();
        }
    }
}